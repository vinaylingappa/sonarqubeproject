package MavenProject.Proj1;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class AppTest 
{
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	@Test
	public void AddTest()
	{
		App objApp = new App();
		int z = objApp.divide(20, 5);
		Assert.assertEquals(4, z);
	}

}